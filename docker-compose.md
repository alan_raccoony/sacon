# http://b.link/sacon-docker-compose

# LAB 3. Docker Compose
목적: 여기서는 도커 컴포즈의 필요성을 이해하고 도커 컴포즈로 기본적인 개발 환경을 구축해봅니다.

## docker-compose 설치하기

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

### 설치 확인
```
docker-compose version
```

## 명령어 축약하기

docker-compose가 너무 길어서 귀찮을 때.

```
# ~/.bashrc 파일 가장 아랫 부분에 다음 내용을 추가
alias dco='docker-compose'
```

```
source ~/.bashrc
dco
```

## 데모: django-sample 이미지로 개발 환경 구축하기(가상 시나리오)

데모 프로젝트: http://b.link/sacon-django-sample

Postgres 서버 실행
```
docker run --rm -d --name postgres \
-p 5432:5432 \
-e POSTGRES_DB=djangosample \
-e POSTGRES_USER=sampleuser \
-e POSTGRES_PASSWORD=samplesecret \
postgres
```

Django 실행
```
docker run -d --rm --name django \
-p 8000:8000 \
--link postgres:db \
django-sample
```

http://IP:8000 에서 확인

Django 마이그레이션과 확인
```
docker exec django-sample ./manage.py migrate
docker exec django-sample ./manage.py showmigrations
```

퇴근 시간이다! 집에 가자!
```
docker kill postgres django
```

다음 날 출근 후 컨테이너들 재실행
```
docker run --rm -d --name postgres \
-p 5432:5432 \
-e POSTGRES_DB=djangosample \
-e POSTGRES_USER=sampleuser \
-e POSTGRES_PASSWORD=samplesecret \
postgres

docker run -d --rm --name django \
-p 8000:8000 \
--link postgres:db \
django-sample
```

데이터 확인
```
docker exec -it django-sample ./manage.py showmigrations
```

데이터가 삭제되지 않게 하려면,
```
docker kill postgres django

docker run --rm -d --name postgres \
-p 5432:5432 \
-e POSTGRES_DB=djangosample \
-e POSTGRES_USER=sampleuser \
-e POSTGRES_PASSWORD=samplesecret \
-v $(pwd)/data:/var/lib/postgresql/data \
postgres

docker run -d --rm --name django -p 8000:8000 django-sample
```

이제 개발을 해봅시다. 소스코드를 조금 변경해보죠.
```
# /app/api/views.py
def message(request):
    return JsonResponse({'message': 'byebye', 'API_HOST': f'{settings.API_HOST}'})
```

잘 바뀌었는지 http://IP:8000/api/message/ 에서 확인합니다.

소스코드가 개발 서버에 바로바로 반영되게 하려면?
```
docker kill django

docker run -d --rm --name django -p 8000:8000 \
-v $(pwd)/app:/app \
django-sample
```

## 실습: Nginx 서버를 도커 컴포즈로 실행하기
Nginx는 널리 사용되는 웹 서버입니다.

### Nginx 서버를 실행하는 docker run 명령어를 작성해보세요
- 호스트의 60080 포트를 컨테이너의 80 포트로 연결하세요.
- index.html 파일을 만들고, Nginx 접속시 이 파일이 나타나게 해보세요.

### 방금 만든 docker run 명령어를 docker-compose.yml로 변환해보세요

## 데모: Ghost 블로그를 도커 컴포즈로 실행하기
Ghost는 간단한 블로깅 시스템입니다. Ghost에서 제공하는 [공식 도커 이미지](https://hub.docker.com/_/ghost/)를 사용하면, 간단하게 Ghost 시스템을 실행할 수 있습니다.

### Ghost 블로그를 실행하는 docker run 명령어를 작성합니다.
`docker run` 명령으로 ghsot 이미지를 실행합니다.
```
docker run -it --name blog --rm -p 2368:2368 ghost
```

브라우저에서 http://IP:2368/ghost 를 열어봅시다.
관리자 설정을 해보고, 글을 새로 작성하거나 샘플 글을 수정해봅시다.

이제, 컨테이너를 종료한 후 앞선 명령을 한 번 더 실행해봅시다.
```
docker kill blog
docker run -d --rm -p 2368:2368 --name blog ghost
```

브라우저를 열어 관리자 페이지(http://IP:2368/ghost)에 가보면, 이전에 설정한 내용이 모두 사라졌음을 확인할 수 있습니다.

현재까지 컨테이너의 모습은 다음과 같습니다.

![](docker-compose/ghost-01.png)

블로그 데이터는 영속적이어야하므로, 데이터가 로컬 디렉터리에 저장되게 해봅시다. 데이터를 저장할 `content` 디렉터리를 만들고,
```
mkdir content
```

다음 명령으로 컨테이너를 실행합니다.
```
docker run -d --rm -p 2368:2368 --name blog -v $(pwd)/content:/var/lib/ghost/content ghost
```

이제 http://IP:62368/ghost 에서 다시 한 번 관리자 설정을 하고, 컨테이너를 종료한 후 다시 실행해봅시다.

볼륨을 연결한 이후의 컨테이너는 다음과 같은 모습입니다.

![](docker-compose/ghost-02.png)

### 도커 컴포즈로 실행하기
앞에서 완성한 도커 명령을 `docker-compose.yml` 파일로 옮겨봅시다.

먼저, 앞서 실행했던 Ghost 컨테이너를 종료합니다.
```
docker kill blog
```

이제 다음과 같이 `docker-compose.yml` 파일을 만듭니다.
```
# docker-compose.yml
version: '3'

services:
  ghost:
    image: ghost
    ports:
      - "2368:2368"
    volumes:
      - ./content:/var/lib/ghost/content
```

`docker-compose.yml` 파일에 적힌 내용을 바탕으로 Ghost 컨테이너를 실행해봅시다.
```
docker-compose up -d
```

http://IP:2368/ghost 에 접속하여 잘 실행되었는지 확인해봅니다.

컨테이너를 종료하려면 다음 명령을 사용합니다.
```
docker-compose down
```

### Ghost 시스템과 Nginx 연결하기
Ghost 시스템은 그 자체로도 잘 작동하지만 앞단에 Nginx 같은 웹 서버를 둠으로써 처리 성능을 높이거나 여타 기능을 사용할 수 있습니다. 이 구조를 `docker-compose.yml`에 구현해봅시다.

```
# docker-compose.yml
version: '3'

services:
  ghost:
    image: ghost
    volumes:
      - ./content:/var/lib/ghost/content
  nginx:
    image: nginx
    volumes: 
      - ./nginx.conf:/etc/nginx/nginx.conf
    ports:
      - 8000:80
```

ghost 서비스 쪽에서 ports가 사라진 것과 nginx 서비스의 포트 연결을 주의 깊게 살펴보세요.

그리고 다음과 같이 `nginx.conf` 파일을 생성합니다.
```
# nginx.conf
events {
  worker_connections 1024;
}

http {
  upstream ghost {
    server ghost:2368;
  }
  
  server {
    listen 80;
    server_name _;

    location / {
      proxy_pass http://ghost;
    }
  }
}
```

이제 도커 컴포즈로 nginx, ghost 서비스를 다시 실행한 후 http://IP:8000 으로 접속할 수 있는지 확인해봅시다.

Nginx 컨테이너를 연결한 모습은 다음과 같습니다.
![](docker-compose/ghost-03-nginx.png)

### 환경변수 추가하기
http://IP:8000/ghost/#/site 에 가보면 미리보기가 제대로 나타나지 않는 것을 확인할 수 있습니다. Ghost 내부의 주소를 나타내는 환경변수의 기본 값이 localhost:8000이기 때문입니다.
`docker-compose.yml` 파일을 다음과 같이 수정합시다. (`ghost` 서비스의 `environment` 부분)
```
# docker-compose.yml
version: '3'

services:
  ghost:
    image: ghost
    volumes:
      - ./content:/var/lib/ghost/content
    environment:
      - url=http://IP:8000
  nginx:
    image: nginx
    volumes: 
      - ./nginx.conf:/etc/nginx/nginx.conf
    ports:
      - 8000:80
```

* ghost 서비스의 환경변수 설정은 https://docs.ghost.org/concepts/config/#section-running-ghost-with-config-env-variables 문서를 참고하였습니다.

파일을 저장하고 컨테이너를 다시 실행합니다.
```
docker-compose down
docker-compose up -d
```

이제 도커 컴포즈로 서비스를 다시 실행한 후 http://IP:8000/ghost/#/site 에서 미리보기가 제대로 나타나는지 확인해봅시다.

## 실습: 워드프레스를 도커 컴포즈로 실행하기
### Wordpress 컨테이너
- 이미지는 wordpress
- 호스트의 60000 포트를 컨테이너의 80 포트로
- WORDPRESS_DB_HOST: db:3306
- WORDPRESS_DB_USER: wordpress
- WORDPRESS_DB_PASSWORD: wordpress
- WORDPRESS_DB_NAME: wordpress
- mysql 컨테이너와 연결

### MySQL 컨테이너
이미지는 mysql:5.7
- MySql 데이터(/var/lib/mysql)는 db_data라는 도커 볼륨에 저장
- MYSQL_ROOT_PASSWORD: somewordpress
- MYSQL_DATABASE: wordpress
- MYSQL_USER: wordpress
- MYSQL_PASSWORD: wordpress

## 데모: 도커 컴포즈로 Django 개발 환경 구성하기

데모 프로젝트: http://b.link/sacon-django-sample

Postgres 컨테이너
```
version: '3'

volumes:
  postgres_data: {}

services:
  postgres:
    image: postgres
    volumes:
      - postgres_data:/var/lib/postgres/data
    environment:
      - POSTGRES_DB=djangosample
      - POSTGRES_USER=sampleuser
      - POSTGRES_PASSWORD=samplesecret
```

Django 컨테이너
```
  django:
    build:
      context: .
      dockerfile: ./compose/django/Dockerfile-dev
    volumes:
      - ./app:/app
    command: ["./manage.py", "runserver", "0:8000"]
    links:
      - postgres:db
    ports:
      - 8000:8000
```

개발 환경 구성을 위한 별도의 Dockerfile
```
# ./compose/django/Dockerfile-dev
FROM python:3

RUN apt-get update && apt -y install libpq-dev
ENV PYTHONUNBUFFERED 1

WORKDIR /app
# COPY    ./app   /app/
COPY    ./app/requirements.txt    /app/
RUN     pip install -r requirements.txt

EXPOSE 8000

# CMD ["python", "manage.py", "runserver", "0:8000"]
```

## 실습: 환경변수 추가시 적용 순서
### Dockerfile-dev 파일에 다음 내용을 추가합니다
- ENV TEST_ENV dockerfile_value
### docker-compose.yml 파일의 django 컨테이너 아래 다음 내용을 추가합니다
```
  django:
    ...
    environment:
      - TEST_ENV=compose_value
```
### 실제 환경변수의 값을 확인합니다
```
$ docker-compose run django bash
echo $TEST_ENV
```

## 실습: 방명록 서비스 실행하기
다음과 같은 구성으로 docker-compose.yml 파일을 작성해보세요.
![](docker-compose/guestbook.png)

### 프론트엔드
- 이미지: raccoony/guestbook-frontend:latest
- 환경변수
	- PORT: 서비스를 실행할 포트
	- GUESTBOOK_API_ADDR: 백엔드 서버의 주소
### 백엔드
- 이미지: raccoony/guestbook-backend:latest
- 환경변수
	- PORT: 서비스를 실행할 포트
	- GUESTBOOK_DB_ADDR: 데이터베이스의 주소
### 데이터베이스
- 이미지: mongo:4
- 포트는 27017
- 서비스가 재시작해도 데이터는 남아 있도록
