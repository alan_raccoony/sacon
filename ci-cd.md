# http://b.link/sacon-ci-cd

# LAB 5. CI/CD
목적: 여기서는 지속적 통합과 지속적 배포의 개념을 살펴본 후 깃랩을 사용하여 파이프라인을 구축해봅니다. 

## 깃랩 가입하기

- https://gitlab.com 접속 후 오른쪽 위의 `Register` 버튼을 클릭합니다.

![](ci-cd/gitlab-main.png)

- `Register` 탭 클릭 후 정보를 입력하고 Register 버튼을 클릭합니다.

**아래 쪽 깃허브 연동 버튼을 사용하면, 나중에 자신의 깃허브 계정에 존재하는 저장소를 가져오기 편합니다.**

![](ci-cd/gitlab-register.png)


## CI/CD용 샘플 프로젝트 클론하기

새 프로젝트 만들기 -> Import Project -> Repo by URL

- Git repository URL: https://gitlab.com/raccoony/ci-sample.git
- Project name: ci-sample
- Project slug: ci-sample
- Visibility Level: Public

**앞으로는 깃랩 화면에서 직접 편집하셔도 됩니다.**

## 첫 CI를 실행해보자!

- 프로젝트 왼쪽 메뉴에서 `CI/CD -> Pipelines`를 선택합니다.

![](ci-cd/gitlab-pipelines-menu.png)

- 오른쪽 위의 `Run Pipeline` 버튼을 클릭합니다.

![](ci-cd/gitlab-pipelines-run.png)

- 옵션 수정 없이 `Run Pipeline` 버튼을 클릭합니다.

![](ci-cd/gitlab-pipelines-run-2.png)

- 초록색 `paased` 아이콘이 보이면 성공! `pep8` 버튼을 클릭합니다.

![](ci-cd/gitlab-pipelines-success.png)

- 다음과 같은 상세 로그를 볼 수 있습니다.

![](ci-cd/gitlab-pipelines-log.png)

## 유닛 테스트 추가하기

- `.gitlab-ci.yml` 파일을 다음과 같이 수정합니다.

```yml
image: docker:latest

stages:
  - test

variables:
  IMAGE_NAME: $CI_REGISTRY_IMAGE/ci-sample
  MYSQL_ROOT_PASSWORD: mypassword
  MYSQL_DATABASE: djangosample
  MYSQL_USER: sampleuser
  MYSQL_PASSWORD: samplesecret

pep8:
  stage: test
  image: python:3.6
  before_script:
    - cd app
    - pip install -r requirements.txt
  script:
    - flake8 --exclude=manage.py --max-line-length=100 .

unit_test:
  stage: test
  services:
    - mysql:5.6
  image: python:3.6
  before_script:
    - cd app
    - pip install -r requirements.txt
  script:
    - python manage.py test
```

- 잡이 실패한 원인을 파악하고, 유닛테스트 잡을 성공시켜 봅시다.

## 테스트 커버리지 확인하고, 배지 붙이기

### 테스트 커버리지 확인하기

`.gitlab-ci.yml`의 `unit_test` 잡 아래에 다음 내용을 추가합니다.

```yml
unit_test:
  ...
  coverage: '/^TOTAL\s+\d+\s+\d+\s+(\d+\%)$/'
  script:
    - coverage run --source='.' manage.py test
    - coverage report
```

잡이 모두 실행된 후 다음과 같이 Coverage가 나타나는지 확인합니다.

### 파이프라인 배지와 커버리지 배지 붙이기

- 프로젝트에서 `Settings -> CI/CD` 메뉴를 선택한 후, `General pipelines` 항목 아래에서 `Pipeline status` 항목을 찾습니다.
- Markdown 부분을 복사하여 `READMD.md` 파일의 제목 부분 아래에 추가합니다. (파이프라인 배지와 커버리지 배지를 각각 복사해야 합니다.)


```markdown
# 도커 환경을 위한 Django 샘플 프로젝트

{여기에 배지 코드를 추가합니다.}

### 요약

...
```

- 프로젝트 메인 화면에 배지가 잘 나타나는지 확인합니다.

## 빌드 자동화

### 도커 이미지 자동으로 빌드하기

- 파이프라인에 빌드 스테이지를 추가합니다. 다음 내용을 `.gitlab-ci.yml`에 추가합니다.

```yml
image: docker:latest

services:
  - docker:dind

stages:
  ...
  - build

build:
  stage: build
  script:
    - docker build -t ci-sample:latest .
```

- 빌드 잡이 성공했는지 확인합니다.

### 빌드한 도커 이미지를 이미지 레지스트리에 저장하기

- `.gitlab-ci.yml`의 build 잡을 다음과 같이 수정합니다.

```yml
build: 
  stage: build
  before_script:
    - docker login -u $GITLAB_USERNAME -p $GITLAB_PASSWORD registry.gitlab.com
  script:
    - docker build -t $CI_REGISTRY_IMAGE/ci-sample:latest .
    - docker push $CI_REGISTRY_IMAGE/ci-sample:latest
```

- 프로젝트에서 `Settings -> CI/CD` 메뉴를 선택한 후, `Variables` 항목을 열고 `GITLAB_USERNAME`과 `GITLAB_PASSWORD`를 추가합니다. 두 값 모두 `Protected`와 `Masked`를 켜줍니다.
- 이제 빌드 잡에서 `docker login ...` 명령이 제대로 실행되고, 이후 이미지 푸시도 성공한 것을 확인합니다.

### 로그인 정보 대신 깃랩 내부 토큰 활용하기

- `.gitlab-ci.yml`의 build 잡에서 `before_script` 부분을 다음과 같이 수정합니다.

```yml
build: 
  stage: build
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
  ...
```

- 빌드 잡에서 도커 로그인과 푸시가 여전히 잘 되는 것을 확인합니다.

### 이미지에 Git 태그 붙이기

- `.gitlab-ci.yml`의 build 잡에서 `script` 부분을 다음과 같이 수정합니다.

```yml
build: 
  ...
  script:
    - docker build -t $CI_REGISTRY_IMAGE/ci-sample:latest -t $CI_REGISTRY_IMAGE/ci-sample:$CI_COMMIT_SHA .
    - docker push $CI_REGISTRY_IMAGE/ci-sample:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE/ci-sample:latest
  ...
```

- 이미지 저장소에 태그가 제대로 올라가는지 확인합니다.
- 중복을 줄이기 위해 다음과 같이 이미지 이름을 변수화합니다.

```yml
variables:
  IMAGE_NAME: $CI_REGISTRY_IMAGE/ci-sample
...
build: 
  ...
  script:
    - docker build -t $IMAGE_NAME:latest -t $IMAGE_NAME:$CI_COMMIT_SHA .
    - docker push $IMAGE_NAME:$CI_COMMIT_SHA
    - docker push $IMAGE_NAME:latest
  ...
```

- `$CI_COMMIT_SHA` 대신 `$CI_COMMIT_SHORT_SHA`를 사용하면 어떻게 되는지 확인해봅시다.
- 깃랩이 제공하는 다른 환경변수들은 <https://docs.gitlab.com/ee/ci/variables/README.html#syntax-of-environment-variables-in-job-scripts>에서 확인할 수 있습니다.

## 배포 자동화

### 배포 잡 추가

- `.gitlab-ci.yml`에 deploy 스테이지와 deploy_to_dev 잡을 다음과 같이 추가합니다.

```yml
stages:
  ...
  - deploy
...
deploy_to_dev:
  stage: deploy
  script:
    - echo deploy_to_dev
```

### 특정 브랜치 이름에 대해서만 작동하는 잡 추가하기

- `.gitlab-ci.yml`의 deploy_to_dev 잡을 다음과 같이 수정합니다.

```yml
...
deploy_to_dev:
  stage: deploy
  script:
    - echo deploy_to_dev
  only:
    - /^feature\/.*$/
```

- `feature/`로 시작하는 브랜치를 만들고 커밋을 추가하여 푸시하면 잡이 잘 작동하는지 확인해봅시다.

### master에 머지됐을 때만 작동하는 잡 추가하기

- `.gitlab-ci.yml`에 deploy_to_staging 잡을 다음과 같이 추가합니다.

```yml
...
deploy_to_staging:
  stage: deploy
  script:
    - echo deploy_to_staging
  only:
    refs:
      - master
```

### master에 머지됐을 때만 작동하는 잡 추가하기

- `.gitlab-ci.yml`에 deploy_to_production 잡을 다음과 같이 추가합니다.

```yml
...
deploy_to_production:
  stage: deploy
  script:
    - echo deploy_to_production
  only:
    refs:
      - master
  when: manual
```

- 이 외에도 잡 실행을 제어하는 다양한 옵션들은 <https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-advanced>에서 확인할 수 있습니다.

## 읽을 거리

- [Postman과 GitLab CI 연동하기 by 리디북스](https://www.ridicorp.com/blog/2017/11/09/postman-gitlab-ci/)
